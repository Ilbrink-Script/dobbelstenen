/**
 * Part 2: Simulate Dice
 */

// handle Gooi! button
const gooi = dontLog => {
    const roll = Math.ceil(Math.random() * 6);
    // if specified skip logging (for testing purposes)
    if (!dontLog) {
        console.log(`Er is ${roll} gegooid`);
        logThrow(roll);
    }
    return roll;
}

// fill array with a specified amount of throws
const doThrows = amount => {
    const resultArr = [];
    const dontLog = true;
    for (let i = 0; i < 8; i++) {
        resultArr.push(gooi(dontLog));
    }
    return resultArr;
}

// do 8 throws and sort the results
const throws = doThrows(8);
console.log("Random worpen", throws);
console.log("Gesorteed", throws.sort());


// keep track of all throws
const throwLog = {};

// (re)initializes the throw log
const initThrowLog = () => {
    for (let i = 1; i <= 6; i++) {
        const startVal = 0;
        throwLog[i] = startVal;
        renderThrow(i, startVal);
    }
};

// adds throw to the throw log
const logThrow = val => {
    const total = ++throwLog[val];
    // console.log(throwLog);
    renderThrow(val, total);
};

// updates the table with the new total
const renderThrow = (val, total) => {
    const cell = document.getElementById(`throw${val}`);
    cell.innerText = total;
};
